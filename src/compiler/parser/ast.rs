pub type Span = (usize, usize);
pub type Expr = (ExprType, Span);

pub const ACC_PUB: i32    = 1 << 0;
pub const ACC_CONST: i32  = 1 << 1;
pub const ACC_STATIC: i32 = 1 << 2; 

#[derive(Debug, PartialEq)]
pub enum ClassType {
    Enum(Type),
    Trait(Type),
    Class(Type),
}

#[derive(Debug, PartialEq)]
pub enum Type {
    Char,
    Short,
    Int,
    Long,
    Byte,
    Ushort,
    Uint,
    Ulong,
    Float,
    Double,
    Bool,
    Void,
    Object(String),
    Generic(String, Vec<Type>),
}

#[derive(Debug, PartialEq)]
pub enum ExprType {
    Null,
    True,
    False,
    Int(i64),
    Float(f64),
    Id(String),
    Str(String),
    Unop(Operator, Box<Expr>),
    Call(Box<Expr>, Vec<Expr>),
    Binop(Operator, Box<(Expr, Expr)>),
    Decl(i32, Type, Vec<(String, Option<Expr>)>),
    Class(i32, ClassType, Vec<Type>, Vec<Expr>),
    Method(i32, Type, String, Vec<Expr>, Vec<Expr>),
}

impl From<String> for Type {
    fn from(type_name: String) -> Type {
        use Type::*;
        match type_name.as_str() {
            "char"   => Char,
            "short"  => Short,
            "int"    => Int,
            "long"   => Long,
            "byte"   => Byte,
            "ushort" => Ushort,
            "uint"   => Uint,
            "ulong"  => Ulong,
            "float"  => Float,
            "double" => Double,
            "bool"   => Bool,
            "void"   => Void,
            _ => Object(type_name),
        }
    }
}

pub mod parser {
    use std::collections::HashSet;
    use std::iter::FromIterator;

    include!(concat!(env!("OUT_DIR"), "/grammar.rs"));

    fn convert_modifiers(modifiers: Vec<String>) -> Result<i32, &'static str> {
        let num_modifiers = modifers.len();
        let modifiers = HashSet::from_iter(modifiers);

        if num_modifiers != modifers.len() {
            Err("Access modifier is repeated")
        } else {
            modifiers.fold(Ok(0), |flags, modifier|
                flags.and_then(|flag| match modifier.as_str() {
                    "pub"    => Ok(flag | ACC_PUB),
                    "const"  => Ok(flag | ACC_CONST),
                    "static" => Ok(flag | ACC_STATIC),
                    _ => Err("Invalid access modifier"),
                })
            )
        }
    }
}